package persistencia.dao.mysql;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.IngresoEgresoDTO;
import persistencia.dao.interfaz.IngresoEgresoDAO;
import utils.ReadForAnio;
import utils.ReadForAnioMes;
import utils.ReadGeneral;
import persistencia.conexion.Conexion;

public class IngresoEgresoDAOMYSQL implements IngresoEgresoDAO
{
	
	private static IngresoEgresoDAOMYSQL instance;
	private static final String insert = "INSERT INTO ingreso_egreso (idIngresoEgreso, titulo, descripcion, fecha, monto) VALUES(?, ?, ?, ?, ?)";
	private static final String update = "UPDATE ingreso_egreso SET titulo=?, descripcion=?, fecha=?, monto=? WHERE idIngresoEgreso=?";
	private static final String delete = "DELETE FROM ingreso_egreso WHERE idIngresoEgreso=?";
	private static final String readAll = "SELECT * FROM ingreso_egreso ORDER BY fecha DESC";
	private static final String readForId = "SELECT * FROM ingreso_egreso WHERE idIngresoEgreso=?";
	private static final String readGeneral = "SELECT "
												+ "anio, "
												+ "ROUND (SUM(ingresos), 2) AS ingreso, "
												+ "ROUND (SUM(egresos), 2) AS egreso, "
												+ "ROUND (SUM(ie), 2) AS ganancia "
											+ "FROM "
												+ "(SELECT "
												+ "YEAR(fecha) AS anio, "
												+ "IF( monto>=0, monto, '' ) as ingresos, "
												+ "IF( monto<0, monto, '' ) as egresos, "
												+ "monto as ie "
												+ "FROM ingreso_egreso) AS subQuery "
											+ "GROUP BY "
												+ "anio DESC";
	
	private static final String readForAnio = "SELECT "
												+ "mes,	"
												+ "ROUND (SUM(ingresos), 2) AS ingreso, "
												+ "ROUND (SUM(egresos), 2) AS egreso, "
												+ "ROUND (SUM(ie), 2) AS ganancia "
											+ "FROM "
												+ "(SELECT "
												+ "YEAR(fecha) AS anio, "
												+ "MONTH(fecha) AS mes, "
												+ "IF( monto>=0, monto, '' ) as ingresos, "
												+ "IF( monto<0, monto, '' ) as egresos, "
												+ "monto as ie "
												+ "FROM ingreso_egreso) AS subQuery "
												+ "WHERE "
												+ "anio = ? "
											+ "GROUP BY "
												+ "mes "
											+ "ORDER BY "
												+ "mes ";

	private static final String readForAnioMes = "SELECT "
													+ "anio, "
													+ "mes, "
													+ "ROUND (SUM(ingresos), 2) AS ingreso,	"
													+ "ROUND (SUM(egresos), 2) AS egreso, "
													+ "ROUND (SUM(ie), 2) AS ganancia "
												+ "FROM "
													+ "(SELECT "
													+ "YEAR(fecha) AS anio, "
													+ "MONTH(fecha) AS mes, "
													+ "IF( monto>=0, monto, '' ) as ingresos, "
													+ "IF( monto<0, monto, '' ) as egresos, "
													+ "monto as ie "
													+ "FROM ingreso_egreso) AS subQuery "
												+ "GROUP BY "
													+ "anio, "
													+ "mes "
												+ "ORDER BY "
													+ "anio DESC, "
													+ "mes DESC";
	
	private static final String readOfMonth = "SELECT * FROM ingreso_egreso WHERE fecha >= ? AND fecha <= ? ORDER BY fecha DESC";
	
	public static IngresoEgresoDAOMYSQL getInstance()
	{
		if (instance == null)
			instance = new IngresoEgresoDAOMYSQL();
		return instance;
	}
	
	@Override
	public boolean insert(IngresoEgresoDTO ingresoEgreso) 
	{
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try 
		{	
			statement = conexion.getSQLConexion().prepareStatement(insert);
			statement.setString(1, ingresoEgreso.getIdIngresoEgreso());
			statement.setString(2, ingresoEgreso.getTitulo());
			statement.setString(3, ingresoEgreso.getDescripcion());
			Date fecha = new Date(ingresoEgreso.getFecha().getTime());
			statement.setDate(4, fecha);
			statement.setBigDecimal(5, ingresoEgreso.getMonto());
			
			if(statement.executeUpdate() > 0) { return true; }
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean update(IngresoEgresoDTO ingresoEgreso) 
	{
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(update);
			statement.setString(1, ingresoEgreso.getTitulo());
			statement.setString(2, ingresoEgreso.getDescripcion());
			Date fecha = new Date(ingresoEgreso.getFecha().getTime());
			statement.setDate(3, fecha);
			statement.setBigDecimal(4, ingresoEgreso.getMonto());
			statement.setString(5, ingresoEgreso.getIdIngresoEgreso());

			if(statement.executeUpdate() > 0)
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean delete(IngresoEgresoDTO ingresoEgreso) 
	{
		PreparedStatement statement;
		int chequeoUpdate = 0;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(delete);
			statement.setString(1, ingresoEgreso.getIdIngresoEgreso());
			chequeoUpdate = statement.executeUpdate();
			if(chequeoUpdate > 0) 
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public List<IngresoEgresoDTO> readAll() 
	{
		PreparedStatement statement;
		ResultSet resultSet;
		ArrayList<IngresoEgresoDTO> ingresosEgresos = new ArrayList<IngresoEgresoDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readAll);
			resultSet = statement.executeQuery();			
			while(resultSet.next())
			{
				IngresoEgresoDTO newIE = new IngresoEgresoDTO();
				newIE.setIdIngresoEgreso( resultSet.getString("idIngresoEgreso") );
				newIE.setTitulo( resultSet.getString("titulo") );
				newIE.setDescripcion( resultSet.getString("descripcion") );
				newIE.setFecha( resultSet.getDate("fecha") );
				newIE.setMonto( resultSet.getBigDecimal("monto") );
			
				ingresosEgresos.add(newIE);
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return ingresosEgresos;
	}

	@Override
	public IngresoEgresoDTO readForId(String idIngresoEgreso) 
	{
		PreparedStatement statement;
		ResultSet resultSet;
		IngresoEgresoDTO ingresosEgresos = new IngresoEgresoDTO();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readForId);
			statement.setString(1, idIngresoEgreso);
			resultSet = statement.executeQuery();			
			
			ingresosEgresos.setIdIngresoEgreso( resultSet.getString("idIngresoEgreso") );
			ingresosEgresos.setTitulo( resultSet.getString("titulo") );
			ingresosEgresos.setDescripcion( resultSet.getString("descripcion") );
			ingresosEgresos.setFecha( resultSet.getDate("fecha") );
			ingresosEgresos.setMonto( resultSet.getBigDecimal("monto") );
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return ingresosEgresos;
	}

	@Override
	public List<ReadGeneral> readGeneral() 
	{
		PreparedStatement statement;
		ResultSet resultSet;
		ArrayList<ReadGeneral> resumeGeneral = new ArrayList<ReadGeneral>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readGeneral);
			resultSet = statement.executeQuery();			
			while(resultSet.next())
			{
				ReadGeneral newRG = new ReadGeneral();
				newRG.setAnio( resultSet.getInt("anio") );
				newRG.setIngreso( resultSet.getBigDecimal("ingreso") );
				newRG.setEgreso( resultSet.getBigDecimal("egreso") );
				newRG.setGanancia( resultSet.getBigDecimal("ganancia") );
				resumeGeneral.add(newRG);
			}

		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return resumeGeneral;
	}
	
	@Override
	public List<ReadForAnio> readForAnio(String anio) 
	{
		PreparedStatement statement;
		ResultSet resultSet;
		List<ReadForAnio> resumeMensual = new ArrayList<ReadForAnio>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readForAnio);
			statement.setString(1, anio);
			resultSet = statement.executeQuery();			
			while(resultSet.next())
			{
				ReadForAnio newFA = new ReadForAnio();
				newFA.setMes( resultSet.getInt("mes") );
				newFA.setIngreso( resultSet.getBigDecimal("ingreso") );
				newFA.setEgreso( resultSet.getBigDecimal("egreso") );
				newFA.setGanancia( resultSet.getBigDecimal("ganancia") );
				
				resumeMensual.add(newFA);
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return resumeMensual;
	}
	
	@Override
	public List<ReadForAnioMes> readForAnioMes() 
	{
		PreparedStatement statement;
		ResultSet resultSet;
		List<ReadForAnioMes> resumeAnioMes = new ArrayList<ReadForAnioMes>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readForAnioMes);
			resultSet = statement.executeQuery();			
			while(resultSet.next())
			{
				ReadForAnioMes newFAM = new ReadForAnioMes();
				newFAM.setAnio( resultSet.getInt("anio") );
				newFAM.setMes( resultSet.getInt("mes") );
				newFAM.setIngreso( resultSet.getBigDecimal("ingreso") );
				newFAM.setEgreso( resultSet.getBigDecimal("egreso") );
				newFAM.setGanancia( resultSet.getBigDecimal("ganancia") );
				
				resumeAnioMes.add(newFAM);
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return resumeAnioMes;
	}
	
	@Override
	public List<IngresoEgresoDTO> readOfMonth(String anio, String mes) 
	{
		PreparedStatement statement;
		ResultSet resultSet;
		ArrayList<IngresoEgresoDTO> ingresosEgresos = new ArrayList<IngresoEgresoDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readOfMonth);
			String fechaIni = anio+"-"+mes+"-"+"01";
			statement.setString(1, fechaIni);
			String fechaFin = anio+"-"+mes+"-"+"31";
			statement.setString(2, fechaFin);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				IngresoEgresoDTO newIE = new IngresoEgresoDTO();
				newIE.setIdIngresoEgreso( resultSet.getString("idIngresoEgreso") );
				newIE.setTitulo( resultSet.getString("titulo") );
				newIE.setDescripcion( resultSet.getString("descripcion") );
				newIE.setFecha( resultSet.getDate("fecha") );
				newIE.setMonto( resultSet.getBigDecimal("monto") );
			
				ingresosEgresos.add(newIE);
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return ingresosEgresos;
	}
	
}