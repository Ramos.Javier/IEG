package presentacion.controlador;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import dto.IngresoEgresoDTO;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import modelo.GestorIngresosEgresos;

public class ControladorVentanaAll 
{
	
	@FXML
	public BorderPane borderPane;
	@FXML
	public TextField txtFiltro;
	@FXML
	public Label lblIngresos;
	@FXML
	public Label lblEgresos;
	@FXML
	public Label lblGanancias;

	@FXML
	public Button btnBuscar;
	@FXML
	public Button btnVolver;

	@FXML
	public TableView<IngresoEgresoDTO> tblIngresosEgresos;

	@FXML
	private TableColumn<String, String> colHeader;
	@FXML
	private TableColumn<IngresoEgresoDTO, String> colTitulo;
	@FXML
	private TableColumn<IngresoEgresoDTO, String> colDescripcion;
	@FXML
	private TableColumn<IngresoEgresoDTO, String> colFecha;
	@FXML
	private TableColumn<IngresoEgresoDTO, String> colMonto;

	public ObservableList<IngresoEgresoDTO> ingresosEgresos;
	public ObservableList<IngresoEgresoDTO> ingresosEgresosFiltrados;

	private BorderPane borderPaneVentanaPrincipal;

	public Double ingresos = 0.00;
	public Double egresos = 0.00;
	public Double ganancias = 0.00;

	public SimpleDateFormat dateFormat;
	
	@SuppressWarnings("deprecation")
	public void initialize( BorderPane borderPaneVentanaPrincipal )
	{
		this.borderPaneVentanaPrincipal = borderPaneVentanaPrincipal;
		
		this.dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	
		this.borderPane.setStyle(" -fx-background-image: url('resources/images/fondoIE.jpg'); -fx-background-size: cover; ");
		
		String title =  "INGRESOS / EGRESOS";
		this.colHeader.setText( title );
		
		Image img1 = new Image("resources/images/buscar.png");
	    ImageView view1 = new ImageView(img1);
	    view1.setFitHeight(20);
	    view1.setFitWidth(20);
	    this.btnBuscar.setGraphic(view1);

		Image img2 = new Image("resources/images/volver.png");
	    ImageView view2 = new ImageView(img2);
	    view2.setFitHeight(20);
	    view2.setFitWidth(20);
	    this.btnVolver.setGraphic(view2);
	    
	    this.txtFiltro.setVisible(false);
	    this.txtFiltro.setPrefWidth(0);
	    
		this.colTitulo.prefWidthProperty().bind( this.tblIngresosEgresos.widthProperty().multiply(.20) );
		this.colDescripcion.prefWidthProperty().bind( this.tblIngresosEgresos.widthProperty().multiply(.50) );
		this.colFecha.prefWidthProperty().bind( this.tblIngresosEgresos.widthProperty().multiply(.15) );
		this.colMonto.prefWidthProperty().bind( this.tblIngresosEgresos.widthProperty().multiply(.15) );
		
		this.colTitulo.setResizable(false);
		this.colDescripcion.setResizable(false);
		this.colFecha.setResizable(false);
		this.colMonto.setResizable(false);

		this.ingresosEgresosFiltrados = FXCollections.observableArrayList( GestorIngresosEgresos.getInstance().readAll() );
        this.ingresosEgresos = FXCollections.observableArrayList( GestorIngresosEgresos.getInstance().readAll() );
		this.tblIngresosEgresos.setItems(ingresosEgresos);
		
		this.actualizarTags();
		
		this.lblIngresos.setText( " $ " + String.format("%,.2f", this.ingresos) ) ;
		this.lblEgresos.setText( " $ " + String.format("%,.2f", this.egresos) );
		this.lblGanancias.setText( " $ " + String.format("%,.2f", this.ganancias) );

		this.tblIngresosEgresos.getStylesheets().add("styles/tblIEAll.css");

		this.colTitulo.setCellValueFactory(new PropertyValueFactory<>("titulo"));
		this.colDescripcion.setCellValueFactory(new PropertyValueFactory<>("descripcion"));
		this.colFecha.setCellValueFactory(
				   ie -> 
				   {
				      SimpleStringProperty property = new SimpleStringProperty();
				      DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				      property.setValue(dateFormat.format(ie.getValue().getFecha()));
				      return property;
				   });
		this.colMonto.setCellValueFactory(
				   ie -> 
				   {
				      SimpleStringProperty property = new SimpleStringProperty();
				      property.setValue(String.format("%,.2f", ie.getValue().getMonto()));
				      return property;
				   });
		this.colHeader.getStyleClass().add("encabezado");
		this.colHeader.setStyle(" -fx-background-color: #000000fa; -fx-alignment: center; ");
		this.colTitulo.setStyle(" -fx-text-fill : #000000; -fx-alignment: center; ");
		this.colDescripcion.setStyle(" -fx-text-fill : #000000; ");
		this.colFecha.setStyle(" -fx-text-fill : #000000; -fx-alignment: center; ");
		this.colMonto.setStyle(" -fx-text-fill : #000000; -fx-alignment: center; ");

		// Deshabilitar el reordenamiento de las columnas.
		this.colHeader.impl_setReorderable(false);
		this.colTitulo.impl_setReorderable(false);
		this.colDescripcion.impl_setReorderable(false);
		this.colFecha.impl_setReorderable(false);
		this.colMonto.impl_setReorderable(false);
		
	}

	@FXML
	public void volver(ActionEvent event) 
	{
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/presentacion/vista/VentanaGeneral.fxml"));
		try	
		{	
			Parent root = loader.load();
			
			ControladorVentanaGeneral contro = loader.getController();
			contro.initialize( this.borderPaneVentanaPrincipal );

			this.borderPaneVentanaPrincipal.setCenter(root);
			
			ControladorVentanaPrincipal.btnAnalisis.setDisable(false);
		}
		catch(Exception ex)	{	System.out.print(ex);	}
	}
	
	@FXML
	public void buscar(ActionEvent event) 
	{
		if ( this.txtFiltro.isVisible() )
		{
		    this.txtFiltro.setVisible(false);
		    this.txtFiltro.setPrefWidth(0);
		    this.txtFiltro.setText("");
		    this.filtrar(null);
		}
		else
		{
		    this.txtFiltro.setVisible(true);
		    this.txtFiltro.setPrefWidth(145);
		    this.txtFiltro.requestFocus();
		}
	}
	
	@FXML
	public void filtrar(KeyEvent event) 
	{
		String nombre = this.txtFiltro.getText().toLowerCase();
		
		if( nombre.isEmpty() )
		{
			this.ingresosEgresosFiltrados.clear();
			this.ingresosEgresosFiltrados = FXCollections.observableArrayList( GestorIngresosEgresos.getInstance().readAll() );
			
			this.tblIngresosEgresos.setItems(this.ingresosEgresosFiltrados);
			this.tblIngresosEgresos.refresh();
		}
		else
		{
			this.ingresosEgresosFiltrados.clear();
			for(IngresoEgresoDTO p : this.ingresosEgresos)
			{
				String title = p.getTitulo();
				String description = p.getDescripcion();
				String fecha = this.dateFormat.format(p.getFecha());
				String cadena = title + " " + description + " " + fecha;
				if( cadena.toLowerCase().contains(nombre) )
				{
					this.ingresosEgresosFiltrados.add(p);
				}
			}
			this.tblIngresosEgresos.setItems(this.ingresosEgresosFiltrados);	
			this.tblIngresosEgresos.refresh();
		}
		this.actualizarTags();
	}
	
	public void actualizarTags()
	{
		this.egresos = 0.00;
		this.ingresos = 0.00;
		this.ganancias = 0.00;
		for(IngresoEgresoDTO ie : this.ingresosEgresosFiltrados)
		{	
			if( ie.getMonto().doubleValue()<0 ) 
			{	this.egresos = this.egresos + ie.getMonto().doubleValue();	}
			else
			{	this.ingresos = this.ingresos + ie.getMonto().doubleValue();	}
		}
		this.ganancias = this.ingresos + this.egresos;
		
		this.lblIngresos.setText( " $ " + String.format("%,.2f", this.ingresos) ) ;
		this.lblEgresos.setText( " $ " + String.format("%,.2f", this.egresos) );
		this.lblGanancias.setText( " $ " + String.format("%,.2f", this.ganancias) );
	}
	
}