package presentacion.controlador;

import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import modelo.GestorIngresosEgresos;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

import dto.IngresoEgresoDTO;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;

public class ControladorAgregarIE
{

	@FXML
	private VBox panelIE;
	@FXML
	private ComboBox<String> comboTipo;
	@FXML
	private Label lblTipo;
	@FXML
	private Label lblTitulo;
	@FXML
	private Label lblMonto;
	@FXML
	private TextField txtTitulo;
	@FXML
	private TextArea txtDescripcion;
	@FXML
	private TextField txtMonto;
	@FXML
	private Button btnRegistrar;

	public ControladorVentanaGestionIngresoEgreso contro;

	public void initialize( ControladorVentanaGestionIngresoEgreso contro )
	{	
		this.contro = contro;
		
		ObservableList<String> listTipo = FXCollections.observableArrayList();
		listTipo.addAll("Ingreso","Egreso");
		this.comboTipo.setItems(listTipo);
		this.comboTipo.setValue("Ingreso");
		this.seleccionDeTipo(null);
		
		// Alineacion en el centro de header ComboBox
		this.comboTipo.setButtonCell(new ListCell<String>() {
		    @Override
		    public void updateItem(String item, boolean empty) {
		        super.updateItem(item, empty);
		        if (item != null) {
		            setText(item);
		            setAlignment(Pos.CENTER);
		            Insets old = getPadding();
		            setPadding(new Insets(old.getTop(), 0, old.getBottom(), 0));
		        }
		    }
		});
		// Alineacion en el centro de la lista ComboBox		
		this.comboTipo.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {
		    @Override
		    public ListCell<String> call(ListView<String> list) {
		        return new ListCell<String>() {
		            @Override
		            public void updateItem(String item, boolean empty) {
		                super.updateItem(item, empty);
		                if (item != null) {
		                    setText(item);
		                    setAlignment(Pos.CENTER);
		                    setPadding(new Insets(3, 3, 3, 0));
		                }
		            }
		        };
		    }
		});
		
		this.lblTipo.setStyle("-fx-text-fill: #ffffff");
		this.lblTitulo.setStyle("-fx-text-fill: #ffffff");
		this.lblMonto.setStyle("-fx-text-fill: #ffffff");
		
		this.txtMonto.textProperty().addListener(new ChangeListener<String>() 
		{
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) 
            {
                if (!newValue.matches("\\d{0,10}([\\.]\\d{0,2})?")) 
                {
                    txtMonto.setText(oldValue);
                }
            }
        });
	}
	
	@FXML
	public void seleccionDeTipo(ActionEvent event) 
	{
		if(this.comboTipo.getValue() == "Ingreso")
		{
			this.panelIE.setStyle("-fx-background-image: url('resources/images/ingreso.jpg'); -fx-background-size: cover; ");
			this.txtTitulo.setPromptText("Ej. Sueldo");
			this.txtMonto.setPromptText("25000.15");
		}
		if(this.comboTipo.getValue() == "Egreso")
		{
			this.panelIE.setStyle("-fx-background-image: url('resources/images/egreso.jpg'); -fx-background-size: cover; ");
			this.txtTitulo.setPromptText("Ej. Alimentos");
			this.txtMonto.setPromptText("300.50");
		}
	}
	
	@FXML
	public void agregarIE(ActionEvent event) 
	{
		IngresoEgresoDTO newIE = new IngresoEgresoDTO();
		newIE.setIdIngresoEgreso( UUID.randomUUID().toString() );
		newIE.setTitulo(this.txtTitulo.getText());
		newIE.setDescripcion(this.txtDescripcion.getText());
		Date fechaAhora = new Date();
		newIE.setFecha( fechaAhora );

		if ( this.txtTitulo.getText().equals("") || this.txtMonto.getText().equals("") )
		{
			this.mostrarAlertError("Titulo y monto no pueden ser vacio.");
		}
		else
		{
			if(this.comboTipo.getValue() == "Ingreso")
			{	
				newIE.setMonto(new BigDecimal(this.txtMonto.getText()));
				GestorIngresosEgresos.getInstance().insert(newIE);
			}
			else if(this.comboTipo.getValue() == "Egreso")
			{
				newIE.setMonto(new BigDecimal("-"+this.txtMonto.getText()));
				GestorIngresosEgresos.getInstance().insert(newIE);
			}
			
			this.contro.ingresosEgresos.clear();
			this.contro.ingresosEgresosFiltrados.clear();
			this.contro.ingresosEgresos = FXCollections.observableArrayList( GestorIngresosEgresos.getInstance().readOfMonth( this.contro.anio, this.contro.mes ) );
			this.contro.ingresosEgresosFiltrados = FXCollections.observableArrayList( GestorIngresosEgresos.getInstance().readOfMonth( this.contro.anio, this.contro.mes ) );
			
			this.contro.tblIngresosEgresos.setItems(this.contro.ingresosEgresosFiltrados);
			this.contro.tblIngresosEgresos.refresh();
			this.contro.txtFiltro.setText("");
			this.contro.actualizarTags();
			
			Stage stage = (Stage) this.btnRegistrar.getScene().getWindow();
			stage.close();
			
		}
	}
	
	private void mostrarAlertError(String mensaje) 
	{
	    Alert alert = new Alert(Alert.AlertType.ERROR);
	    alert.setHeaderText(null);
	    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image("/resources/images/ieg.png"));
	    alert.setTitle("Error");
	    alert.setContentText( mensaje );
	    alert.showAndWait();
	}

}