package presentacion.controlador;

import java.io.IOException;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import modelo.GestorIngresosEgresos;
import utils.ReadForAnioMes;

public class ControladorVentanaGeneral 
{
	@FXML
	public BorderPane borderPane;
	
	@FXML
	public TextField txtFiltro;

	@FXML
	public Label lblIngresos;
	@FXML
	public Label lblEgresos;
	@FXML
	public Label lblGanancias;

	@FXML
	public Button btnBuscar;
	@FXML
	public Button btnCharts;
	@FXML
	public Button btnRegistros;

	@FXML
	public TableView<ReadForAnioMes> tblIngresosEgresos;

	@FXML
	private TableColumn<String, String> colHeader;
	@FXML
	private TableColumn<ReadForAnioMes, String> colAnio;
	@FXML
	private TableColumn<ReadForAnioMes, String> colMes;
	@FXML
	private TableColumn<ReadForAnioMes, String> colIngresos;
	@FXML
	private TableColumn<ReadForAnioMes, String> colEgresos;
	@FXML
	private TableColumn<ReadForAnioMes, String> colGanancias;
	
	public ObservableList<ReadForAnioMes> ingresosEgresos;
	public ObservableList<ReadForAnioMes> ingresosEgresosFiltrados;

	public Double ingresos = 0.00;
	public Double egresos = 0.00;
	public Double ganancias = 0.00;
	
	public BorderPane borderPaneVentanaPrincipal;

	public Tooltip tooltip = new Tooltip();
	
	@SuppressWarnings("deprecation")
	public void initialize(BorderPane borderPaneVentanaPrincipal)
	{
		this.borderPaneVentanaPrincipal = borderPaneVentanaPrincipal;
		
		this.borderPane.setStyle(" -fx-background-image: url('resources/images/fondoIE.jpg'); -fx-background-size: cover; ");
		
		this.tooltip.setStyle("-fx-text-fill: orange;");
		
		String title = "General";
		this.colHeader.setText(title.toUpperCase());
		
		Image img1 = new Image("resources/images/buscar.png");
	    ImageView view1 = new ImageView(img1);
	    view1.setFitHeight(20);
	    view1.setFitWidth(20);
	    this.btnBuscar.setGraphic(view1);

		Image img2 = new Image("resources/images/charts.png");
	    ImageView view2 = new ImageView(img2);
	    view2.setFitHeight(20);
	    view2.setFitWidth(20);
	    this.btnCharts.setGraphic(view2);
	    
		Image img3 = new Image("resources/images/all.png");
	    ImageView view3 = new ImageView(img3);
	    view3.setFitHeight(20);
	    view3.setFitWidth(20);
	    this.btnRegistros.setGraphic(view3);	    
	    
	    this.txtFiltro.setVisible(false);
	    this.txtFiltro.setPrefWidth(0);

		this.ingresosEgresosFiltrados = FXCollections.observableArrayList( GestorIngresosEgresos.getInstance().readForAnioMes() );
        this.ingresosEgresos = FXCollections.observableArrayList( GestorIngresosEgresos.getInstance().readForAnioMes() );
		this.tblIngresosEgresos.setItems(ingresosEgresos);
		
		this.actualizarTags();

		this.lblIngresos.setText( " $ " + String.format("%,.2f", this.ingresos) ) ;
		this.lblEgresos.setText( " $ " + String.format("%,.2f", this.egresos) );
		this.lblGanancias.setText( " $ " + String.format("%,.2f", this.ganancias) );

		this.tblIngresosEgresos.getStylesheets().add("styles/tblIEGeneral.css");
		
		this.colAnio.setCellValueFactory(new PropertyValueFactory<>("anio"));
		this.colMes.setCellValueFactory(
				   ie -> 
				   {
				      SimpleStringProperty property = new SimpleStringProperty();
				      property.setValue(this.obtenerMes(ie.getValue().getMes()));
				      return property;
				   });
		this.colIngresos.setCellValueFactory(
				   ie -> 
				   {
				      SimpleStringProperty property = new SimpleStringProperty();
				      property.setValue(String.format("%,.2f", ie.getValue().getIngreso()));
				      return property;
				   });
		this.colEgresos.setCellValueFactory(
				   ie -> 
				   {
				      SimpleStringProperty property = new SimpleStringProperty();
				      property.setValue(String.format("%,.2f", ie.getValue().getEgreso()));
				      return property;
				   });
		this.colGanancias.setCellValueFactory(
				   ie -> 
				   {
				      SimpleStringProperty property = new SimpleStringProperty();
				      property.setValue(String.format("%,.2f", ie.getValue().getGanancia()));
				      return property;
				   });

		this.colHeader.getStyleClass().add("encabezado");
		this.colHeader.setStyle(" -fx-background-color: #000000fa; -fx-alignment: center; ");
		this.colAnio.setStyle(" -fx-text-fill : #000000; -fx-alignment: center; ");
		this.colMes.setStyle(" -fx-text-fill : #000000; -fx-alignment: center; ");
		this.colIngresos.setStyle(" -fx-text-fill : #000000; -fx-alignment: center; ");
		this.colEgresos.setStyle(" -fx-text-fill : #000000; -fx-alignment: center; ");
		this.colGanancias.setStyle(" -fx-text-fill : #000000; -fx-alignment: center; ");

		// Deshabilitar el reordenamiento de las columnas.
		this.colHeader.impl_setReorderable(false);
		this.colAnio.impl_setReorderable(false);
		this.colMes.impl_setReorderable(false);
		this.colIngresos.impl_setReorderable(false);
		this.colEgresos.impl_setReorderable(false);
		this.colGanancias.impl_setReorderable(false);
		
	}

	public void actualizarTags()
	{
		this.egresos = 0.00;
		this.ingresos = 0.00;
		this.ganancias = 0.00;
		for(ReadForAnioMes ie : this.ingresosEgresosFiltrados)
		{	
			this.ingresos = this.ingresos + ie.getIngreso().doubleValue();
			this.egresos = this.egresos + ie.getEgreso().doubleValue();
		}
		this.ganancias = this.ingresos + this.egresos;
		
		this.lblIngresos.setText( " $ " + String.format("%,.2f", this.ingresos) ) ;
		this.lblEgresos.setText( " $ " + String.format("%,.2f", this.egresos) );
		this.lblGanancias.setText( " $ " + String.format("%,.2f", this.ganancias) );
	}
	
	@FXML
	public void buscar(ActionEvent event) 
	{
		if ( this.txtFiltro.isVisible() )
		{
		    this.txtFiltro.setVisible(false);
		    this.txtFiltro.setPrefWidth(0);
		    this.txtFiltro.setText("");
		    this.filtrar(null);
		}
		else
		{
		    this.txtFiltro.setVisible(true);
		    this.txtFiltro.setPrefWidth(145);
		    this.txtFiltro.requestFocus();
		}
	}
	
	@FXML
	public void filtrar(KeyEvent event) 
	{
		String nombre = this.txtFiltro.getText().toLowerCase();
		
		if( nombre.isEmpty() )
		{
			this.ingresosEgresosFiltrados.clear();
			this.ingresosEgresosFiltrados = FXCollections.observableArrayList( GestorIngresosEgresos.getInstance().readForAnioMes() );
			
			this.tblIngresosEgresos.setItems(this.ingresosEgresosFiltrados);
			this.tblIngresosEgresos.refresh();
		}
		else
		{
			this.ingresosEgresosFiltrados.clear();
			for(ReadForAnioMes p : this.ingresosEgresos)
			{
				String cadena = String.valueOf(p.getAnio()) + " " + this.obtenerMes( p.getMes() );
				if( cadena.toLowerCase().contains(nombre) )
				{
					this.ingresosEgresosFiltrados.add(p);
				}
			}
			this.tblIngresosEgresos.setItems(this.ingresosEgresosFiltrados);	
			this.tblIngresosEgresos.refresh();
		}
		this.actualizarTags();
	}
	
	@FXML
	public void charts(ActionEvent event) 
	{
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/presentacion/vista/VentanaCharts.fxml"));
		try	
		{	
			Parent root = loader.load();
			
			ControladorVentanaCharts contro = loader.getController();
			contro.initialize( this.borderPaneVentanaPrincipal );

			this.borderPaneVentanaPrincipal.setCenter(root);
		}
		catch(Exception ex)	{	System.out.print(ex);	}
		
		ControladorVentanaPrincipal.btnAnalisis.setDisable(true);
	}

	@FXML
	public void verRegistros(ActionEvent event) 
	{
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/presentacion/vista/VentanaAll.fxml"));
		try	
		{	
			Parent root = loader.load();
			
			ControladorVentanaAll contro = loader.getController();
			contro.initialize( this.borderPaneVentanaPrincipal );

			this.borderPaneVentanaPrincipal.setCenter(root);
			
			ControladorVentanaPrincipal.btnAnalisis.setDisable(true);
		}
		catch(Exception ex)	{	System.out.print(ex);	}
	}

	@FXML
	public void verMensualidad(MouseEvent event) 
	{
		if( event.getClickCount() == 2  && this.tblIngresosEgresos.getSelectionModel().getSelectedItem() != null )
		{
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/presentacion/vista/VentanaMensual.fxml"));
			
			try 
			{
				Parent root = loader.load();
				
				ControladorVentanaMensual contro = loader.getController();
				contro.initialize(this.borderPaneVentanaPrincipal, this.tblIngresosEgresos.getSelectionModel().getSelectedItem());

				this.borderPaneVentanaPrincipal.setCenter(root);

				ControladorVentanaPrincipal.btnAnalisis.setDisable(true);
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}
        }
	}
	
	private String obtenerMes(int mes)
	{
		if ( mes == 1 ) { return "Enero"; }
		else if ( mes == 2 ) { return "Febrero"; }
		else if ( mes == 3 ) { return "Marzo"; }
		else if ( mes == 4 ) { return "Abril"; }
		else if ( mes == 5 ) { return "Mayo"; }
		else if ( mes == 6 ) { return "Junio"; }
		else if ( mes == 7 ) { return "Julio"; }
		else if ( mes == 8 ) { return "Agosto"; }
		else if ( mes == 9 ) { return "Septiembre"; }
		else if ( mes == 10 ) { return "Octubre"; }
		else if ( mes == 11 ) { return "Noviembre"; }
		else { return "Diciembre"; }
	}

	@FXML
	public void infoRegistros( MouseEvent event )
	{
		String mensaje = "Registros";
		this.tooltip.setText(mensaje);
		this.btnRegistros.setTooltip(tooltip);
	}
	

	@FXML
	public void infoAnalisis( MouseEvent event )
	{
		String mensaje = "An�lisis";
		this.tooltip.setText(mensaje);
		this.btnCharts.setTooltip(tooltip);
	}
	
}