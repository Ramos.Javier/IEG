package presentacion.controlador;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Optional;

import dto.IngresoEgresoDTO;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import modelo.GestorIngresosEgresos;

public class ControladorVentanaGestionIngresoEgreso 
{
	@FXML
	public BorderPane borderPane;
	@FXML
	public TextField txtFiltro;

	@FXML
	public Label lblIngresos;
	@FXML
	public Label lblEgresos;
	@FXML
	public Label lblGanancias;

	@FXML
	public Button btnBuscar;
	@FXML
	public Button btnAgregar;
	@FXML
	public Button btnEditar;
	@FXML
	public Button btnEliminar;

	@FXML
	public TableView<IngresoEgresoDTO> tblIngresosEgresos;

	@FXML
	private TableColumn<String, String> colHeader;
	@FXML
	private TableColumn<IngresoEgresoDTO, String> colTitulo;
	@FXML
	private TableColumn<IngresoEgresoDTO, String> colDescripcion;
	@FXML
	private TableColumn<IngresoEgresoDTO, String> colFecha;
	@FXML
	private TableColumn<IngresoEgresoDTO, String> colMonto;

	public ObservableList<IngresoEgresoDTO> ingresosEgresos;
	public ObservableList<IngresoEgresoDTO> ingresosEgresosFiltrados;

    public DateFormat dateFormat;
	
	public String anio;
	public String mes;
	public Double ingresos = 0.00;
	public Double egresos = 0.00;
	public Double ganancias = 0.00;
	
	@SuppressWarnings("deprecation")
	public void initialize()
	{
	    this.borderPane.setStyle(" -fx-background-image: url('resources/images/fondoIE.jpg'); -fx-background-size: cover; ");

		this.dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		Image img1 = new Image("resources/images/buscar.png");
	    ImageView view1 = new ImageView(img1);
	    view1.setFitHeight(20);
	    view1.setFitWidth(20);
	    this.btnBuscar.setGraphic(view1);

	    Image img2 = new Image("resources/images/agregar.png");
	    ImageView view2 = new ImageView(img2);
	    view2.setFitHeight(20);
	    view2.setFitWidth(20);
	    this.btnAgregar.setGraphic(view2);

	    Image img3 = new Image("resources/images/editar.png");
	    ImageView view3 = new ImageView(img3);
	    view3.setFitHeight(20);
	    view3.setFitWidth(20);
	    this.btnEditar.setGraphic(view3);
	    
	    Image img4 = new Image("resources/images/eliminar.png");
	    ImageView view4 = new ImageView(img4);
	    view4.setFitHeight(20);
	    view4.setFitWidth(20);
	    this.btnEliminar.setGraphic(view4);

	    this.txtFiltro.setVisible(false);
	    this.txtFiltro.setPrefWidth(0);

		this.colTitulo.prefWidthProperty().bind( this.tblIngresosEgresos.widthProperty().multiply(.20) );
		this.colDescripcion.prefWidthProperty().bind( this.tblIngresosEgresos.widthProperty().multiply(.50) );
		this.colFecha.prefWidthProperty().bind( this.tblIngresosEgresos.widthProperty().multiply(.15) );
		this.colMonto.prefWidthProperty().bind( this.tblIngresosEgresos.widthProperty().multiply(.15) );
		
		this.colTitulo.setResizable(false);
		this.colDescripcion.setResizable(false);
		this.colFecha.setResizable(false);
		this.colMonto.setResizable(false);
		
		this.btnAgregar.setDisable(false);
		this.btnEditar.setDisable(true);
		this.btnEliminar.setDisable(true);
		
		// LLenado la tabla
		Date date = new Date();
        ZoneId timeZone = ZoneId.systemDefault();
        LocalDate getLocalDate = date.toInstant().atZone(timeZone).toLocalDate();
        this.mes = String.valueOf( getLocalDate.getMonth().getValue() );
        this.anio = String.valueOf( getLocalDate.getYear() );

        this.ingresosEgresos = FXCollections.observableArrayList( GestorIngresosEgresos.getInstance().readOfMonth( this.anio, this.mes ) );
		this.tblIngresosEgresos.setItems(ingresosEgresos);		
		this.ingresosEgresosFiltrados = FXCollections.observableArrayList( GestorIngresosEgresos.getInstance().readOfMonth( this.anio, this.mes ) );	

		this.actualizarTags();
		
		String title = this.obtenerMes(this.mes);
		this.colHeader.setText(title.toUpperCase());

		this.tblIngresosEgresos.getStylesheets().add("styles/tblIngresosEgresos.css");

		this.colTitulo.setCellValueFactory(new PropertyValueFactory<>("titulo"));
		this.colDescripcion.setCellValueFactory(new PropertyValueFactory<>("descripcion"));		
		this.colFecha.setCellValueFactory(
				   ie -> 
				   {
				      SimpleStringProperty property = new SimpleStringProperty();
				      DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				      property.setValue(dateFormat.format(ie.getValue().getFecha()));
				      return property;
				   });
		this.colMonto.setCellValueFactory(
				   ie -> 
				   {
				      SimpleStringProperty property = new SimpleStringProperty();
				      property.setValue(String.format("%,.2f", ie.getValue().getMonto()));
				      return property;
				   });
		
		this.colHeader.getStyleClass().add("encabezado");
		this.colHeader.setStyle(" -fx-background-color: #000000fa; -fx-alignment: center; ");
		this.colTitulo.setStyle(" -fx-text-fill : #000000; -fx-alignment: center; ");
		this.colDescripcion.setStyle(" -fx-text-fill : #000000; ");
		this.colFecha.setStyle(" -fx-text-fill : #000000; -fx-alignment: center; ");
		this.colMonto.setStyle(" -fx-text-fill : #000000; -fx-alignment: center; ");

		// Deshabilitar el reordenamiento de las columnas.
		this.colHeader.impl_setReorderable(false);
		this.colTitulo.impl_setReorderable(false);
		this.colDescripcion.impl_setReorderable(false);
		this.colFecha.impl_setReorderable(false);
		this.colMonto.impl_setReorderable(false);
		
	}

	@FXML
	public void agregarIE(ActionEvent event) 
	{
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/presentacion/vista/VentanaAgregarIE.fxml"));	
		try 
		{
			Parent root = loader.load();
			
			Scene scene = new Scene(root);
			Stage stage = new Stage();
		
			ControladorAgregarIE contro = loader.getController();
		
			contro.initialize(this);
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(scene);
			stage.getIcons().add(new Image("/resources/images/ieg.png"));
			stage.setTitle("I - E = G");
			stage.setResizable(false);
			
			Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
			stage.setX( (primScreenBounds.getWidth() - 216) / 2 );
			stage.setY((primScreenBounds.getHeight() - 314) / 2);
			
			stage.showAndWait();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	@FXML
	public void editarIE(ActionEvent event) 
	{
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/presentacion/vista/VentanaEditarIE.fxml"));	
		try 
		{
			Parent root = loader.load();
			
			Scene scene = new Scene(root);
			Stage stage = new Stage();
		
			ControladorEditarIE contro = loader.getController();
		
			contro.initialize(this, this.tblIngresosEgresos.getSelectionModel().getSelectedItem());
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(scene);
			stage.getIcons().add(new Image("/resources/images/ieg.png"));
			stage.setTitle("I - E = G");
			stage.setResizable(false);
			
			Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
			stage.setX( (primScreenBounds.getWidth() - 216) / 2 );
			stage.setY((primScreenBounds.getHeight() - 314) / 2);
			
			stage.showAndWait();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	@FXML
	public void eliminarIE(ActionEvent event) 
	{
		if (this.mostrarAlertConfirmation(null, this.tblIngresosEgresos.getSelectionModel().getSelectedItem()).get() == ButtonType.OK)
	    {			
			GestorIngresosEgresos.getInstance().delete(this.tblIngresosEgresos.getSelectionModel().getSelectedItem());
			this.ingresosEgresosFiltrados.clear();
			this.ingresosEgresos.clear();
			this.ingresosEgresosFiltrados = FXCollections.observableArrayList( GestorIngresosEgresos.getInstance().readOfMonth( this.anio, this.mes ) );
			this.ingresosEgresos = FXCollections.observableArrayList( GestorIngresosEgresos.getInstance().readOfMonth( this.anio, this.mes ) );
			this.tblIngresosEgresos.setItems(this.ingresosEgresos);
			this.tblIngresosEgresos.refresh();
			this.txtFiltro.setText("");
			this.actualizarTags();
	    }
	}	

	private Optional<ButtonType> mostrarAlertConfirmation(ActionEvent event, IngresoEgresoDTO ie) 
	{
	    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
	    alert.setHeaderText(null);
	    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image("/resources/images/ieg.png"));
        
        Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
		stage.setX( (primScreenBounds.getWidth() - 434) / 2 );
		stage.setY( (primScreenBounds.getHeight() - 142) / 2 );
        
	    alert.setTitle("Confirmacion");
	    alert.setContentText("� Deseas realmente eliminar '" + ie.getTitulo() +"' ?");
	    
	    return alert.showAndWait();
	}
	
	@FXML
	public void habilitarAcciones(MouseEvent event) 
	{
		if( this.tblIngresosEgresos.getSelectionModel().getSelectedIndex() != -1 )
		{ 
			this.btnEditar.setDisable(false);
			this.btnEliminar.setDisable(false); 
		}
		else
		{ 
			this.btnEditar.setDisable(true);
			this.btnEliminar.setDisable(true);
		}
	}
	
	private String obtenerMes(String mes)
	{
		if ( mes.equals("1") ) { return "Enero"; }
		else if ( mes.equals("2") ) { return "Febrero"; }
		else if ( mes.equals("3") ) { return "Marzo"; }
		else if ( mes.equals("4") ) { return "Abril"; }
		else if ( mes.equals("5") ) { return "Mayo"; }
		else if ( mes.equals("6") ) { return "Junio"; }
		else if ( mes.equals("7") ) { return "Julio"; }
		else if ( mes.equals("8") ) { return "Agosto"; }
		else if ( mes.equals("9") ) { return "Septiembre"; }
		else if ( mes.equals("10") ) { return "Octubre"; }
		else if ( mes.equals("11") ) { return "Noviembre"; }
		else { return "Diciembre"; }
	}
	
	@FXML
	public void buscar(ActionEvent event) 
	{
		if ( this.txtFiltro.isVisible() )
		{
		    this.txtFiltro.setVisible(false);
		    this.txtFiltro.setPrefWidth(0);
		    this.txtFiltro.setText("");
		    this.filtrar(null);
		}
		else
		{
		    this.txtFiltro.setVisible(true);
		    this.txtFiltro.setPrefWidth(145);
		    this.txtFiltro.requestFocus();
		}
	}
	
	@FXML
	public void filtrar(KeyEvent event) 
	{
		this.btnEditar.setDisable(true);
		this.btnEliminar.setDisable(true);
		
		String nombre = this.txtFiltro.getText().toLowerCase();
		
		if( nombre.isEmpty() )
		{
			this.ingresosEgresosFiltrados.clear();
			this.ingresosEgresosFiltrados = FXCollections.observableArrayList( GestorIngresosEgresos.getInstance().readOfMonth( this.anio, this.mes ) );
			
			this.tblIngresosEgresos.setItems(this.ingresosEgresosFiltrados);
			this.tblIngresosEgresos.refresh();
		}
		else
		{
			this.ingresosEgresosFiltrados.clear();
			for(IngresoEgresoDTO p : this.ingresosEgresos)
			{
				String title = p.getTitulo();
				String description = p.getDescripcion();
				String fecha = dateFormat.format(p.getFecha());
				String cadena = title + " " + description + " " + fecha;
				if( cadena.toLowerCase().contains(nombre) )
				{
					this.ingresosEgresosFiltrados.add(p);
				}
			}
			this.tblIngresosEgresos.setItems(this.ingresosEgresosFiltrados);	
			this.tblIngresosEgresos.refresh();
		}
		this.actualizarTags();
	}
	
	public void actualizarTags()
	{
		this.egresos = 0.00;
		this.ingresos = 0.00;
		this.ganancias = 0.00;
		for(IngresoEgresoDTO ie : this.ingresosEgresosFiltrados)
		{	
			if( ie.getMonto().doubleValue()<0 ) 
			{	this.egresos = this.egresos + ie.getMonto().doubleValue();	}
			else
			{	this.ingresos = this.ingresos + ie.getMonto().doubleValue();	}
		}
		this.ganancias = this.ingresos + this.egresos;
		
		this.lblIngresos.setText( " $ " + String.format("%,.2f", this.ingresos) ) ;
		this.lblEgresos.setText( " $ " + String.format("%,.2f", this.egresos) );
		this.lblGanancias.setText( " $ " + String.format("%,.2f", this.ganancias) );
	}
		
}