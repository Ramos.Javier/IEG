package modelo;

import java.util.List;
import dto.IngresoEgresoDTO;
import persistencia.dao.mysql.IngresoEgresoDAOMYSQL;
import utils.ReadForAnio;
import utils.ReadForAnioMes;
import utils.ReadGeneral;

public class GestorIngresosEgresos 
{
	
	private static GestorIngresosEgresos instance;
	private IngresoEgresoDAOMYSQL ingresoEgresoDaoSQL;
	
	private GestorIngresosEgresos() 
	{
		this.ingresoEgresoDaoSQL = IngresoEgresoDAOMYSQL.getInstance();
	}

	public static GestorIngresosEgresos getInstance() 
	{
		if ( instance == null )
			instance = new GestorIngresosEgresos();
		return instance;
	}

	public void insert(IngresoEgresoDTO ingresoEgreso) 
	{
		this.ingresoEgresoDaoSQL.insert(ingresoEgreso);
	}
	
	public void delete(IngresoEgresoDTO ingresoEgreso)
	{
		this.ingresoEgresoDaoSQL.delete(ingresoEgreso);
	}
	
	public void update(IngresoEgresoDTO ingresoEgreso)
	{
		this.ingresoEgresoDaoSQL.update(ingresoEgreso);
	}

	public List<IngresoEgresoDTO> readAll() 
	{
		return this.ingresoEgresoDaoSQL.readAll();
	}
	
	public IngresoEgresoDTO readForId(String idIngresoEgreso)
	{
		return this.ingresoEgresoDaoSQL.readForId(idIngresoEgreso);
	}
	
	public List<ReadGeneral> readGeneral()
	{
		return this.ingresoEgresoDaoSQL.readGeneral();
	}
	
	public List<ReadForAnio> readForAnio(String anio) 
	{
		return this.ingresoEgresoDaoSQL.readForAnio(anio);
	}
	
	public List<ReadForAnioMes> readForAnioMes() 
	{
		return this.ingresoEgresoDaoSQL.readForAnioMes();
	}
	
	public List<IngresoEgresoDTO> readOfMonth(String anio, String mes) 
	{
		return this.ingresoEgresoDaoSQL.readOfMonth(anio, mes);
	}
	
}