package utils;

public class TableMes 
{

	private String mes;
	private String perdida;
	private String ganancia;

	public String getMes() 
	{
		return mes;
	}
	
	public void setMes(String mes) 
	{
		this.mes = mes;
	}
	
	public String getPerdida() 
	{
		return perdida;
	}
	
	public void setPerdida(String perdida) 
	{
		this.perdida = perdida;
	}
	
	public String getGanancia() 
	{
		return ganancia;
	}
	
	public void setGanancia(String ganancia) 
	{
		this.ganancia = ganancia;
	}
		
}